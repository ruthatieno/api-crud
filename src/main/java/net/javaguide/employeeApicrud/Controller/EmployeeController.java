package net.javaguide.employeeApicrud.Controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import net.javaguide.employeeApicrud.Model.Employee;
import net.javaguide.employeeApicrud.Service.EmployeeService;


@RestController
@RequestMapping("/api/")
public class EmployeeController {
	@Autowired
private EmployeeService service;

   
   @PostMapping("v1/Employee")
   public ResponseEntity<Employee>saveEmployee(@RequestBody Employee employee){
	 
	return new ResponseEntity<Employee>(service.saveEmployee(employee),(HttpStatus.CREATED));
	
   }
   @GetMapping("v1/Employee")
   public List<Employee>getAll(){
	   return service.getAll();
	   
   }
   @GetMapping("v1/Employee/{Id}")
   public ResponseEntity<Employee> getEmployeeById(@PathVariable int Id)
   {
	return  new ResponseEntity<>(service.getEmployeeById(Id),(HttpStatus.OK));
	   
   }
   
  
   @PutMapping("v1/Employee/{Id}")
   public ResponseEntity<Employee>updateEmployee(@PathVariable int Id,@RequestBody Employee employee) {
       try {
    	 
           return new ResponseEntity<Employee>(service.updateEmployee(Id,employee),(HttpStatus.OK));
       } catch (NoSuchElementException e) {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
}
}
   @DeleteMapping("v1/Employee/{Id}")
   public void deleteEmployee(@PathVariable Integer Id) {
	   service.deleteEmployee(Id);
	   
   }

}
