package net.javaguide.employeeApicrud.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employer")
public class Employee {
	 @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column
      private Integer Id;
	 @Column
      private String firstname;
	 @Column
      private String lastname;
	 @Column
      private String email;
	 @Column
      private String gender;
	public Integer getId() {
		return Id;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}
