package net.javaguide.employeeApicrud.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import net.javaguide.employeeApicrud.Model.*;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee ,Integer> {

}
