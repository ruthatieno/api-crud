package net.javaguide.employeeApicrud.Service;

import java.util.List;

import net.javaguide.employeeApicrud.Model.Employee;

public interface EmployeeServiceImp {
	 List<Employee> getAll();
	  Employee saveEmployee(Employee employee);
	 Employee getEmployeeById(Integer Id);
	 Employee updateEmployee(Integer Id,Employee employee);
	 void deleteEmployee(Integer Id);
	 void findById(Integer Id);
	 void updateEmployee(Employee employee);

}
