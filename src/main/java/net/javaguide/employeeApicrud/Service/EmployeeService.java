package net.javaguide.employeeApicrud.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.javaguide.employeeApicrud.Model.Employee;
import net.javaguide.employeeApicrud.Repository.EmployeeRepository;
@Service
public class EmployeeService implements EmployeeServiceImp{
@Autowired
private EmployeeRepository repository;
	@Override
	public List<Employee> getAll() {
		return repository.findAll();
	}

	@Override
	public Employee saveEmployee(Employee employee) {
	 return this.repository.save(employee);
		
	}

	@Override
	public Employee getEmployeeById(Integer Id) {
		Optional<Employee>optional=repository.findById(Id);
		Employee employee=null;
		if(optional.isPresent()) {
			employee=optional.get();
			
		}else {
			throw new RuntimeException("Employee not found for id::" + Id);
			
		}
		return employee;
	}

	
	public Employee updateEmployee(Integer Id,Employee employee) {
		employee=repository.findById(Id).get();
		Optional<Employee>optional=repository.findById(Id);
		if(optional.isPresent()) {
			employee=optional.get();
			 this.repository.save(employee);
			
		}else {
			throw new RuntimeException("Employee not found for id::" + Id);
			
		}
		return employee;
		
		
	}

	@Override
	public void deleteEmployee(Integer Id) {
		this.repository.deleteById(Id);
		
	}

	@Override
	public void findById(Integer Id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}



}
